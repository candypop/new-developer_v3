
1. RTFM: Recently, I had a conversation with a colleague about programming exercises. While I was explaining the algorithm and the mathematics behind it, he asked me to repeat the problem... Which I answered by resending him the link to the exercise.
2. LMGTFY: a few days ago an old friend asked me a general english question that could be easily answered by a quick Google search. Instead of providing the answer directly, I sent them a Google link.

languages: javascript, typescript, java, python
OS: macOS
