
Developers typically work together on a share repository, frequently adding code several time a  day. Using CI (Continuous Integration),
we can automate buid and testing processes. These are triggered whenever new changes are commited to the repository. This helps in identifying errors early in the development process and ensures that the system is always in a deployable state.

A common CD code follows this pattern:
    - Main function that orchestrates the running of unit tests, integration tests, and deployment to production.
    - Run unit tests function that runs the actual tests and returns true if all pass
    - Run integration tests function runs the actual tests and returns true if all pass
    - Deploy function handles deployment
    - Logger handles error and general message logging
