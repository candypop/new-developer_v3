export function findMatchingPair(nums: number[], sum: number): number[] {
    if (!sum) return [];
    if (nums.length < 1) return [];
  
    const map = new Map();
  
    for (let i = 0; i < nums.length; i++) {
      const complement = sum - nums[i];
      if (map.has(complement)) {
        const pair = nums[map.get(complement)];
        return [pair, nums[i]];
      } else {
        map.set(nums[i], i);
      }
    }
    return [];
  }
  