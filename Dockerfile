FROM node:alpine
WORKDIR /rebaja-tus-cuentas
ENV NODE_ENV=development
COPY package.json .
RUN npm install \
    && npm install typescript -g
COPY . .
RUN tsc
RUN npx jest
CMD ["node", "./dist/index.js"]