import { findMatchingPair } from "../src/question4"

describe('matching pair function tests', ()=>{
    test('empty array should result in empty array as output', ()=>{
        expect(findMatchingPair([], 2)).toEqual([])
    })

    test('pair found', ()=>{
        expect(findMatchingPair([2,3,6,7], 9)).toEqual([3,6])
    })

    test('pair not found ', ()=>{
        expect(findMatchingPair([1,3,3,7], 9)).toEqual([])
    })

    test('empty sum arg returns empty array', ()=>{
        expect(findMatchingPair([4,2,5,3], null)).toEqual([])
    })


    test('empty args returns empty array', ()=>{
        expect(findMatchingPair([], null)).toEqual([])
    })
})

